# The Democratic Hierarchy

### What is The Hierarchy?

The Hierarchy is a new form of democratic organization, whose constitution is drafted as such as to ensure a true democracy (better than representative democracy, and closer to direct democracy).  
In short, the mode of organization is by creating a bottom-up hierarchy of scrum teams.  
Each team is responsible for delivering the results they themselves set to deliver during one year, and they are also responsible for collectively choosing, defining and following-up the goals and objectives of the entire hierarchical organization.


### The guiding principles of the organization
1. The purpose of the organization is to ensure that ALL members have the following basic rights fulfilled:
  - fair/decent/basic access to housing and clothing
  - fair/decent/basic access to food and drink
  - fair/decent/basic access to health-care
  - fair/decent/basic access to the internet
  - fair/decent/basic access to education
  - fair/decent/basic access to leisure time (including sports and entertainment)
  - fair/decent/basic access to justice and security  
  How do we define "fair/decent/basic"? This is for the members of the organization to discuss and agree, and can of course vary over time.  
2. All of these must be provided in a sustainable way, with respect for the natural environment and all the other living beings (animals and plants).
3. Justice and security
  - TBD
4. Dealing with external threats
  - TBD
  - All members must show solidarity when it comes to dealing with external threats, even if they are not directed at themselves personally.


### How is it organized?

Based on these very simple rules:
1. All the members of the organization group themselves freely (preferably based on personal affinities) into teams of minimum 6 and maximum 10 people. The number of members should preferably be even.
2. Once formed, a team has to be maintained for at least one year. Team members will then be able (after one year) to stay within the same team for another whole year, or to move to another team (or even to start a completely new team) for another whole year.
3. It is preferable that the members of a team have various professional qualifications, to complement each other's skills. The team as a whole must be able to provide on its own a specific service, task or project that might be required or requested to it by the higher society.
4. Any team must contain at least one senior member (having more than 5 years of experience in her/his profession) and at least one junior member (having less than 1 year of experience in his/her profession).
5. The teams are self-organizing and self-managed in regard to how they choose to achieve their goals and objectives for the year. At the start of the year they must choose goals and objectives for the following year, based on the general goals of the entire hierarchy (and of course following the guiding principles of the organisation!)
6. Each team selects one of their members to represent them in the higher level of the hierarchy.
7. The selected representative has a mandate of ONLY 1 YEAR. After one year, another member of the team MUST be chosen to represent the team. It is preferable that, by rotation, each member of the team will get the chance to represent their team. Also, even if a team member moves to another team in the future years, (s)he IS NOT ALLOWED to be elected as team-representative during the course of the following 5 years. (Of course, they can user their previous "leading" experience in order to persuade and influence the decisions of the elected team-representative, but has no formal or informal power to enforce or dictate the actions and decisions of the elected representatives).
8. The selected representatives can then freely organise themselves into teams, based on the same principles as the teams from the lower level.
9. The goals of these higher-level teams are more generic, and they will set the directions for defining the goals and objectives of all the teams "under their umbrella".
10. Based on the same rule, each of the higher-level teams selects one of their members to represent them in another level above them.
11. The process is repeated until the top of the hierarchy is composed of only one team. This team is composed of an odd number of members, minimum 5 and maximum 9, and the team must adhere to ALL the rules already defined above.
12. The entire history of the processes (elections, goals, objectives, setting-up and actual implementation) is tracked in a distributed ledger that is accessible to (and replicated on) all the devices of the members of the organization, so there can be no "cheating". Also, each member will have the right (and responsability) to follow the correct functioning of the organization, the correctness, effectiveness and efficiency of the implementation of the adopted goals and objectives, the following of rules by everyone (s)he is in contact with, and the fairness and just treatment of every member of the organization. If you see something inefficient, propose to improve it. If you see someone stealing or cheating, remember that they are stealing from you. If you see something wrong, don't ignore it!


